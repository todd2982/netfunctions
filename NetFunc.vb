﻿'This file is part of NETFunc.dll.

'NETFunc.dll is free software: you can redistribute it and/or modify
'it under the terms of the GNU Lesser Public License as published by
'the Free Software Foundation, either version 3 of the License, or
'(at your option) any later version.

'NETFunc.dll is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
'GNU Lesser Public License for more details.

'You should have received a copy of the GNU Lesser Public License
'along with NETFunc.dll.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Net.Sockets
Imports System.Text
Imports System.Net
Imports System.Net.Dns

Public Class NetFunc

    Public Shared Function TCPPortStatus(ByVal Host As String, ByVal Port As Integer, ByVal Wait As Integer) As Boolean
        Dim soc As Socket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        Try
            soc.SendTimeout = Wait
            soc.ReceiveTimeout = Wait
            soc.Connect(Host, Port)
            Return True
            soc.Close()
        Catch ex As Exception
            Return False
        End Try
    End Function

    'Public Shared Function UDPPortStatus(ByVal Host As String, ByVal Port As Integer, ByVal Wait As Integer) As Boolean
    '    Dim result As Boolean = True
    '    Dim udpClient As New UdpClient(Host, Port)
    '    Dim RecvClient As New UdpClient(Port)
    '    Dim RemoteEndpoint As New IPEndPoint(Host, Port)
    '    Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes("test")
    '    Try
    '        udpClient.Send(sendBytes, sendBytes.Length)
    '        Dim recvBytes As [Byte]() = RecvClient.Receive(RemoteEndpoint)
    '        'Dim returnData As String = Encoding.ASCII.ToString(recvBytes)

    '    Catch e As Exception
    '        result = False
    '        udpClient.Close()
    '        RecvClient.Close()
    '    End Try
    '    Return result

    'End Function

    Public Shared Function PingComputer(ByVal Target As String) As Boolean
        'Returns True/False value if host is online via ping. If not online return false.
        'If network unavalable return false. If NUL return false.
        Dim result As Boolean
        result = False
        If Target <> "" Then
            Try
                result = My.Computer.Network.Ping(Target)
            Catch exc As Exception
                result = False
            End Try
        Else
            result = False
        End If
        Return (result)
    End Function

    <Obsolete("Don't use this routine any more. Use the GetARecord " & _
    "instead.")> _
    Public Shared Function GetIP(ByVal Target As String) As String
        '###############
        'Renaming to GetARecord
        '###############
        ' currently only lists the first ip resolved. if multiple addresses exist they will not be returned.
        ' example: google.com
        Dim ips() As IPAddress
        ips = GetHostEntry(Target).AddressList
        Return ips(0).ToString
    End Function

    Public Shared Function GetARecord(ByVal Target As String) As String
        '###############
        'Replacing GetIP function.
        '###############
        ' currently only lists the first ip resolved. if multiple addresses exist they will not be returned.
        ' example: google.com
        Dim ips() As IPAddress
        ips = GetHostEntry(Target).AddressList
        Return ips(0).ToString
    End Function

    Public Shared Function GetPTRRecord(ByVal IP As String) As String
        'Returns hostname as string from IP address as string. On error, return "UNKNOWN" Reverse lookups...
        Dim res As String
        res = Nothing

        Try
            res = GetHostEntry(IP).HostName
        Catch ex As Exception
            res = "UNKNOWN"
        End Try

        Return res
    End Function
End Class
